/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <getopt.h>
#include <poll.h>
#include <string>
#include <wait.h>
#include <unistd.h>

#include <libgen.h>
#include <vulkan_device.hpp>

constexpr unsigned int EXEC_PATH_MAX_LEN = 1024;

static std::string TEST_CASES[] = {
   "soft_recovery_loop", "soft_recovery_pagefault_read", "soft_recovery_pagefault_write",
   "hard_reset_cp_wait", "hard_reset_dma_out_of_bounds", "hard_reset_dma_use_after_free",
};

constexpr option options[] = {
   {"help", no_argument, nullptr, 0},
   {"list-tests", no_argument, nullptr, 0},
   {"test-dir", required_argument, nullptr, 0},
   {"dummy", required_argument, nullptr, 0},
   {"test-filter", required_argument, nullptr, 0},
   {"stress", no_argument, nullptr, 0},
};

bool runTest(const std::string& testName, const std::string& testDir, const std::string& dummyPath);
void runStress(const std::string& testDir, const std::string& dummyPath, const std::string& testFilter);

int main(int argc, char** argv) {
   char exec_path[EXEC_PATH_MAX_LEN];
   exec_path[EXEC_PATH_MAX_LEN - 1] = '\0';
   readlink("/proc/self/exe", exec_path, EXEC_PATH_MAX_LEN);
   if (exec_path[EXEC_PATH_MAX_LEN - 1] != '\0') {
      fprintf(stderr, "Executable path too long, the test search directory might be wrong!\n");
      exec_path[EXEC_PATH_MAX_LEN - 1] = '\0';
   }
   dirname(exec_path);

   std::string testDir = std::string(exec_path) + "/";
   std::string dummyPath = std::string(exec_path) + "/dummy";
   std::string testFilter = "";
   bool stress = false;

   int optIdx;
   while (getopt_long(argc, argv, "", options, &optIdx) != -1) {
      switch (optIdx) {
      case 0:
         printf(
            "Usage: hangtest [--help] [--test-dir <directory>] [--dummy <exe>] [--test-filter <filter>] [--stress]\n"
            "\n"
            "Options summary:\n"
            "--help                    show this message\n"
            "--test-dir <directory>    override search directory for test executables\n"
            "--dummy <exe>             override executable for dummy subprocess\n"
            "--test-filter <filter>    only execute tests containing filter string\n"
            "--stress                  stress test mode: dummy process will continuously submit GPU work in parallel "
            "to the tests\n");
         return 0;
      case 1:
         for (const auto& test : TEST_CASES)
            printf("%s\n", test.c_str());
         return 0;
      case 2: testDir = std::string(optarg) + "/"; break;
      case 3: dummyPath = optarg; break;
      case 4: testFilter = optarg; break;
      case 5: stress = true; break;
      default: fprintf(stderr, "See --help for a list of options.\n");
      }
   }

   bool success = true;
   bool is_dgpu;
   {
      VulkanDevice device;
      is_dgpu = device.properties().deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
      fprintf(stderr, "Running on device \"%s\"\n", device.properties().deviceName);
   }

   if (stress) {
      fprintf(stderr, "Running in stress test mode, skipping hard reset tests.\n");
      runStress(testDir, dummyPath, testFilter);
      /* Returning from the stress test means we failed */
      success = false;
   } else {
      if (is_dgpu) {
         fprintf(stderr, "Running on a discrete GPU, skipping hard reset tests.\n");
      }
      for (auto& name : TEST_CASES) {
         if (!testFilter.empty() && name.find(testFilter) == std::string::npos)
            continue;
         if (is_dgpu && name.find("hard") != std::string::npos)
            continue;

         success &= runTest(name, testDir, dummyPath);
      }
   }

   return success ? 0 : 1;
}

pid_t launchSubprocess(const char* path, char* const* arguments) {
   pid_t res = fork();
   if (res == -1) {
      perror("fork() failed");
      abort();
   }
   if (!res) {
      if (execv(path, arguments)) {
         fprintf(stderr, "could not exec %s!\n", path);
         abort();
      }
   }
   return res;
}

#define COLOR_RESET  "\033[0m"
#define COLOR_RED    "\033[31m"
#define COLOR_GREEN  "\033[1;32m"
#define COLOR_YELLOW "\033[1;33m"

/* Runs a test and checks for success.
 * A test is only considered successful if both the test process and the dummy process return with exit code 0.
 */
bool runTest(const std::string& testName, const std::string& testDir, const std::string& dummyPath) {
   int pipeFDs[2];
   if (pipe(pipeFDs)) {
      perror("Failed to create pipe");
      return false;
   }

   bool success = true;
   const char* resultDescription = " (GPU hang handled)";

   char dummyName[] = "dummy";
   std::string fdNames[2] = {
      std::to_string(pipeFDs[0]),
      std::to_string(pipeFDs[1]),
   };
   char* dummyArgs[] = {
      dummyName,
      fdNames[0].data(),
      fdNames[1].data(),
      NULL,
   };
   pid_t dummyPID = launchSubprocess(dummyPath.c_str(), dummyArgs);

   pollfd wait_poll = {
      .fd = pipeFDs[0],
      .events = POLLIN,
   };
   if (!poll(&wait_poll, 1, 3000)) {
      fprintf(stderr, "error: dummy process failed to initialize!\n");
      close(pipeFDs[0]);
      close(pipeFDs[1]);
      return false;
   }
   char pipeBuffer = 0;
   /* Clear the read end */
   read(pipeFDs[0], &pipeBuffer, 1);

   std::string testPath = testDir + testName;
   char* name = strdup(testName.data());
   char* testArgs[] = {
      name,
      NULL,
   };

   pid_t testPID = launchSubprocess(testPath.c_str(), testArgs);

   free(name);

   pid_t waitedPID;
   int waitStatus;
   do {
      waitedPID = wait(&waitStatus);

      if (WIFSTOPPED(waitStatus) || WIFCONTINUED(waitStatus))
         continue;

      if (waitedPID == dummyPID) {
         success = false;
         resultDescription = " (Innocent context killed)";
      }
   } while (waitedPID != testPID);

   /* If the test subprocess errors, the hang was not reported correctly. */
   if (success && !(WIFEXITED(waitStatus) && WEXITSTATUS(waitStatus) == 0)) {
      if (WIFSIGNALED(waitStatus) && WTERMSIG(waitStatus) == SIGKILL) {
         /* The Deck has udev rules that SIGKILL processes guilty of a hard reset.
          * If the test process gets SIGKILLed, it's likely that it triggered this rule instead of a genuine bug.
          */
         const char* warnString = isatty(2) ? COLOR_YELLOW "WARNING" COLOR_RESET : "WARNING";
         fprintf(stderr, "%s: Guilty context killed by SIGKILL. This might be a bug (unless this is a Steam Deck)\n",
                 warnString);
      } else {
         success = false;
         resultDescription = WIFEXITED(waitStatus) ? " (No hang reported)" : " (Guilty context crashed)";
      }
   }

   if (success) {
      /* Write something nonzero to poke the dummy process */
      char data = 1;
      write(pipeFDs[1], &data, 1);

      int dummy_status;
      wait(&dummy_status);

      while (WIFSTOPPED(dummy_status) || WIFCONTINUED(dummy_status))
         wait(&dummy_status);

      if (!WIFEXITED(dummy_status) || WEXITSTATUS(dummy_status) != 0) {
         success = false;
         resultDescription = " (Innocent context killed)";
      }
   }

   close(pipeFDs[0]);
   close(pipeFDs[1]);

   const char* resultStr;
   if (success)
      resultStr = isatty(1) ? COLOR_GREEN "OK" COLOR_RESET : "OK";
   else
      resultStr = isatty(1) ? COLOR_RED "FAILED" COLOR_RESET : "FAILED";
   printf("%-30s %s%s\n", testName.c_str(), resultStr, resultDescription);
   return success;
}

void runStress(const std::string& testDir, const std::string& dummyPath, const std::string& testFilter) {
   char dummyName[] = "dummy";
   char stressArg[] = "--stress";
   char* dummyArgs[] = {
      dummyName,
      stressArg,
      NULL,
   };
   pid_t dummyPID = launchSubprocess(dummyPath.c_str(), dummyArgs);

   bool fail = false;
   const char* failReason;
   std::string failedTestName;
   while (!fail) {
      for (auto& test : TEST_CASES) {
         if (test.find(testFilter) == std::string::npos)
            continue;
         /* Hard resets can't guarantee that innocent contexts stay alive if they submit work in parallel,
          * so skip them in the stress test.
          */
         if (test.find("hard") != std::string::npos)
            continue;

         std::string testPath = testDir + test;
         char* name = test.data();
         char* testArgs[] = {
            name,
            NULL,
         };

         pid_t testPID = launchSubprocess(testPath.c_str(), testArgs);

         pid_t waitedPID;
         int waitStatus;
         do {
            waitedPID = wait(&waitStatus);

            if (WIFSTOPPED(waitStatus) || WIFCONTINUED(waitStatus))
               continue;

            if (waitedPID == dummyPID) {
               fail = true;
               failReason = " (Innocent context killed)";
               failedTestName = test;
               break;
            }
         } while (waitedPID != testPID);

         if (fail)
            break;

         /* If the test subprocess errors, the hang was not reported correctly. */
         if (!(WIFEXITED(waitStatus) && WEXITSTATUS(waitStatus) == 0)) {
            fail = true;
            failReason = WIFEXITED(waitStatus) ? " (No hang reported)" : " (Guilty context crashed)";
            failedTestName = test;
            break;
         }
      }
   }

   kill(dummyPID, SIGTERM);

   const char* failedText = isatty(1) ? COLOR_RED "FAILED" COLOR_RESET : "FAILED";
   fprintf(stderr, "Stress test %s in test %s %s", failedText, failedTestName.c_str(), failReason);
}

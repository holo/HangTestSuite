/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "vulkan_device.hpp"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>

uint32_t prefix_sum_spv[] = {
#include <shaders/prefix_sum.comp.spv.h>
};

int main(int argc, char** argv) {
   if (argc < 2)
      return 1;

   bool stress = false;
   if (!strcmp(argv[1], "--stress"))
      stress = true;

   int pipeFDs[2] = {};

   if (argc >= 3) {
      pipeFDs[0] = atoi(argv[1]);
      pipeFDs[1] = atoi(argv[2]);
   }

   if ((!pipeFDs[0] || !pipeFDs[1]) && !stress)
      return 1;

   VulkanDevice device;

   Buffer dummyBuf = device.createBuffer(65536 * sizeof(uint32_t));
   memset(dummyBuf.hostMap, 0, 65536 * sizeof(uint32_t));

   Pipeline prefixPipeline = device.createPipeline(prefix_sum_spv, sizeof(prefix_sum_spv), 8);

   auto commandBuffer = device.createCommandBuffer(QueueType::ACE);

   auto fence = device.createFence();

   VkCommandBufferBeginInfo beginInfo = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
   };
   vkBeginCommandBuffer(commandBuffer.buffer, &beginInfo);

   vkCmdBindPipeline(commandBuffer.buffer, VK_PIPELINE_BIND_POINT_COMPUTE, prefixPipeline.pipeline);
   vkCmdPushConstants(commandBuffer.buffer, prefixPipeline.layout, VK_SHADER_STAGE_COMPUTE_BIT, 0, 8, &dummyBuf.va);

   vkCmdDispatch(commandBuffer.buffer, 1024, 0, 0);

   CHECK_VKRESULT(vkEndCommandBuffer(commandBuffer.buffer));

   if (!stress) {
      /* Signal to parent process that device initialization is done, then wait for the parent process */
      char data = 0;
      /* Since we only use one pipe for communication, we could read our own written data
       * instead of waiting for the parent write. Check if what we read was our own write and
       * try again if it was. */
      do {
         write(pipeFDs[1], &data, 1);
         usleep(1000);
         read(pipeFDs[0], &data, 1);
      } while (data == 0);
   }

   VkResult result;
   do {
      VkSubmitInfo submitInfo = {
         .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
         .commandBufferCount = 1,
         .pCommandBuffers = &commandBuffer.buffer,
      };
      result = vkQueueSubmit(device.aceQueue(), 1, &submitInfo, fence);
      if (result != VK_ERROR_DEVICE_LOST) {
         CHECK_VKRESULT(result);
         result = vkWaitForFences(device.device(), 1, &fence, VK_TRUE, UINT64_MAX);
      }
   } while (stress && result == VK_SUCCESS);

   device.destroyPipeline(prefixPipeline);
   device.destroyCommandBuffer(commandBuffer);
   device.destroyBuffer(dummyBuf);
   device.destroyFence(fence);

   if (result != VK_SUCCESS)
      fprintf(stderr, "failure in dummy\n");

   close(pipeFDs[0]);
   close(pipeFDs[1]);

   return result == VK_SUCCESS ? 0 : 1;
}
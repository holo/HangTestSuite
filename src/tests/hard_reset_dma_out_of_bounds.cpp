/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <vulkan_device.hpp>

/*
 * Test triggering a hard reset by hanging the CP
 * Triggers a CP DMA transfer that overruns the allocated memory region
 */

int main() {
   VulkanDevice device;

   auto buffer = device.createBuffer(0x1000);

   auto commandBuffer = device.createCommandBuffer(QueueType::GFX);

   auto fence = device.createFence();

   VkCommandBufferBeginInfo beginInfo = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
   };
   vkBeginCommandBuffer(commandBuffer.buffer, &beginInfo);

   vkCmdFillBuffer(commandBuffer.buffer, buffer.buffer, 0x1000 - 2048 + 4, 2048, 0);

   CHECK_VKRESULT(vkEndCommandBuffer(commandBuffer.buffer));

   VkResult result;
   VkSubmitInfo submitInfo = {
      .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
      .commandBufferCount = 1,
      .pCommandBuffers = &commandBuffer.buffer,
   };
   CHECK_VKRESULT(vkQueueSubmit(device.gfxQueue(), 1, &submitInfo, fence));

   result = vkWaitForFences(device.device(), 1, &fence, VK_TRUE, UINT64_MAX);

   /* If a submission kills the GPU, RADV only returns VK_ERROR_DEVICE_LOST on the next submission. */
   if (result != VK_ERROR_DEVICE_LOST) {
      result = vkQueueSubmit(device.gfxQueue(), 1, &submitInfo, fence);
   }

   device.destroyFence(fence);
   device.destroyCommandBuffer(commandBuffer);

   device.destroyBuffer(buffer);

   /* Only mark the test as successful if the device actually hung. */
   return result == VK_ERROR_DEVICE_LOST ? 0 : 1;
}

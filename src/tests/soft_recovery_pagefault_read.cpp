/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <vulkan_device.hpp>

uint32_t loop_shader_spv[] = {
#include <shaders/infinite_loop.comp.spv.h>
};

/*
 * Test triggering soft recovery by causing lots of page faults (writing to address 0 in an infinite loop)
 */

int main() {
   VulkanDevice device;

   auto pipeline = device.createPipeline(loop_shader_spv, sizeof(loop_shader_spv), 8);

   auto commandBuffer = device.createCommandBuffer(QueueType::GFX);

   auto fence = device.createFence();

   VkCommandBufferBeginInfo beginInfo = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
   };
   vkBeginCommandBuffer(commandBuffer.buffer, &beginInfo);

   VkDeviceAddress null = 0;
   vkCmdBindPipeline(commandBuffer.buffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline.pipeline);
   vkCmdPushConstants(commandBuffer.buffer, pipeline.layout, VK_SHADER_STAGE_COMPUTE_BIT, 0, 8, &null);

   vkCmdDispatch(commandBuffer.buffer, 64, 64, 64);

   CHECK_VKRESULT(vkEndCommandBuffer(commandBuffer.buffer));

   VkResult result;
   VkSubmitInfo submitInfo = {
      .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
      .commandBufferCount = 1,
      .pCommandBuffers = &commandBuffer.buffer,
   };
   CHECK_VKRESULT(vkQueueSubmit(device.gfxQueue(), 1, &submitInfo, fence));

   result = vkWaitForFences(device.device(), 1, &fence, VK_TRUE, UINT64_MAX);

   /* If a submission kills the GPU, RADV only returns VK_ERROR_DEVICE_LOST on the next submission. */
   if (result != VK_ERROR_DEVICE_LOST) {
      result = vkQueueSubmit(device.gfxQueue(), 1, &submitInfo, fence);
   }

   device.destroyFence(fence);
   device.destroyCommandBuffer(commandBuffer);
   device.destroyPipeline(pipeline);

   /* Only mark the test as successful if the device actually hung. */
   return result == VK_ERROR_DEVICE_LOST ? 0 : 1;
}

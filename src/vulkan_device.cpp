/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <vulkan_device.hpp>
#include <vector>

template <typename HandleType, typename ResultType>
using EnumeratePFN = void(VKAPI_PTR*)(HandleType type, uint32_t* count, ResultType* pEnumerants);

template <typename Handle, typename ResultElement>
std::vector<ResultElement> enumerate(Handle handle, EnumeratePFN<Handle, ResultElement> pfnEnumerate) {
   uint32_t valueCount;

   pfnEnumerate(handle, &valueCount, nullptr);
   std::vector<ResultElement> values = std::vector<ResultElement>(valueCount);
   pfnEnumerate(handle, &valueCount, values.data());

   return values;
}

VulkanDevice::VulkanDevice() {
   CHECK_VKRESULT(volkInitialize());

   VkApplicationInfo applicationInfo = {
      .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
      .pApplicationName = "GPU Hang Test Suite",
      .applicationVersion = VK_MAKE_API_VERSION(0, 1, 0, 0),
      .apiVersion = VK_MAKE_API_VERSION(0, 1, 3, 0),
   };
   VkInstanceCreateInfo instanceCreateInfo = {
      .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
      .pApplicationInfo = &applicationInfo,
   };
   CHECK_VKRESULT(vkCreateInstance(&instanceCreateInfo, nullptr, &m_instance));

   volkLoadInstanceOnly(m_instance);

   /* Just choose the first physical device. If necessary, use MESA_VK_DEVICE_SELECT to switch devices. */
   uint32_t deviceCount = 1;
   vkEnumeratePhysicalDevices(m_instance, &deviceCount, &m_physDevice);

   vkGetPhysicalDeviceProperties(m_physDevice, &m_properties);

   VkPhysicalDeviceMemoryProperties memoryProperties;
   vkGetPhysicalDeviceMemoryProperties(m_physDevice, &memoryProperties);

   m_memTypeIdx = -1u;
   for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; ++i) {
      if ((memoryProperties.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) &&
          (memoryProperties.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)) {
         m_memTypeIdx = i;
         break;
      }
   }

   std::vector<VkQueueFamilyProperties> queue_families =
      enumerate(m_physDevice, vkGetPhysicalDeviceQueueFamilyProperties);

   for (uint32_t i = 0; i < queue_families.size(); ++i) {
      if (queue_families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
         m_gfxFamilyIdx = i;
      else if (queue_families[i].queueFlags & VK_QUEUE_COMPUTE_BIT)
         m_aceFamilyIdx = i;
   }

   float prio = 1.0f;
   VkDeviceQueueCreateInfo queueCreateInfos[2] = {
      {
         .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
         .queueFamilyIndex = m_gfxFamilyIdx,
         .queueCount = 1,
         .pQueuePriorities = &prio,
      },
      {
         .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
         .queueFamilyIndex = m_aceFamilyIdx,
         .queueCount = 1,
         .pQueuePriorities = &prio,
      },
   };

   VkPhysicalDeviceVulkan12Features vk12features = {
      .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES,
      .bufferDeviceAddress = true,
   };

   VkPhysicalDeviceFeatures2 features = {
      .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
      .pNext = &vk12features,
   };

   VkDeviceCreateInfo deviceCreateInfo = {
      .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
      .pNext = &features,
      .queueCreateInfoCount = 2,
      .pQueueCreateInfos = queueCreateInfos,
   };
   CHECK_VKRESULT(vkCreateDevice(m_physDevice, &deviceCreateInfo, nullptr, &m_device));

   volkLoadDevice(m_device);

   vkGetDeviceQueue(m_device, m_gfxFamilyIdx, 0, &m_gfxQueue);
   vkGetDeviceQueue(m_device, m_aceFamilyIdx, 0, &m_aceQueue);
}

Buffer VulkanDevice::createBuffer(VkDeviceSize size) {
   uint32_t familyIndices[2] = { m_gfxFamilyIdx, m_aceFamilyIdx };
   VkBufferCreateInfo createInfo = {
      .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
      .size = size,
      .usage = VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
      .sharingMode = VK_SHARING_MODE_CONCURRENT,
      .queueFamilyIndexCount = 2,
      .pQueueFamilyIndices = familyIndices,
   };
   VkBuffer buffer;

   CHECK_VKRESULT(vkCreateBuffer(m_device, &createInfo, nullptr, &buffer));

   VkMemoryRequirements requirements;
   vkGetBufferMemoryRequirements(m_device, buffer, &requirements);

   VkDeviceMemory memory;
   VkMemoryAllocateFlagsInfo flagsInfo = {
      .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO,
      .flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT,
   };
   VkMemoryAllocateInfo allocateInfo = {
      .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
      .pNext = &flagsInfo,
      .allocationSize = requirements.size,
      .memoryTypeIndex = m_memTypeIdx,
   };
   CHECK_VKRESULT(vkAllocateMemory(m_device, &allocateInfo, nullptr, &memory));

   vkBindBufferMemory(m_device, buffer, memory, 0);

   VkBufferDeviceAddressInfo addressInfo = {
      .sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
      .buffer = buffer,
   };
   VkDeviceAddress va = vkGetBufferDeviceAddress(m_device, &addressInfo);

   void *hostMap;
   CHECK_VKRESULT(vkMapMemory(m_device, memory, 0, VK_WHOLE_SIZE, 0, &hostMap));

   return {
      .buffer = buffer,
      .memory = memory,
      .va = va,
      .hostMap = hostMap,
   };
}

void VulkanDevice::destroyBuffer(Buffer& buffer) {
   vkUnmapMemory(m_device, buffer.memory);
   vkDestroyBuffer(m_device, buffer.buffer, nullptr);
   vkFreeMemory(m_device, buffer.memory, nullptr);
}

CommandBuffer VulkanDevice::createCommandBuffer(QueueType type) {
   VkCommandPoolCreateInfo poolCreateInfo = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
      .queueFamilyIndex = type == QueueType::GFX ? m_gfxFamilyIdx : m_aceFamilyIdx,
   };
   VkCommandPool pool;
   CHECK_VKRESULT(vkCreateCommandPool(m_device, &poolCreateInfo, nullptr, &pool));

   VkCommandBufferAllocateInfo allocateInfo = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
      .commandPool = pool,
      .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      .commandBufferCount = 1,
   };
   VkCommandBuffer buffer;
   CHECK_VKRESULT(vkAllocateCommandBuffers(m_device, &allocateInfo, &buffer));

   return {
      .pool = pool,
      .buffer = buffer,
   };
}


void VulkanDevice::destroyCommandBuffer(CommandBuffer& commandBuffer) {
   vkDestroyCommandPool(m_device, commandBuffer.pool, nullptr);
}

Pipeline VulkanDevice::createPipeline(uint32_t* spv, uint32_t spvSize, uint32_t pushConstantSize) {
   VkShaderModuleCreateInfo moduleCreateInfo = {
      .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
      .codeSize = spvSize,
      .pCode = spv,
   };
   VkShaderModule module;
   CHECK_VKRESULT(vkCreateShaderModule(m_device, &moduleCreateInfo, nullptr, &module));

   VkPushConstantRange range = {
      .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
      .size = pushConstantSize,
   };
   VkPipelineLayoutCreateInfo layoutCreateInfo = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      .pushConstantRangeCount = 1,
      .pPushConstantRanges = &range,
   };
   VkPipelineLayout layout;
   CHECK_VKRESULT(vkCreatePipelineLayout(m_device, &layoutCreateInfo, nullptr, &layout));

   VkComputePipelineCreateInfo pipelineCreateInfo = {
      .sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
      .stage =
         {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .stage = VK_SHADER_STAGE_COMPUTE_BIT,
            .module = module,
            .pName = "main",
         },
      .layout = layout,
   };
   VkPipeline pipeline;
   CHECK_VKRESULT(vkCreateComputePipelines(m_device, VK_NULL_HANDLE, 1, &pipelineCreateInfo, nullptr, &pipeline));

   vkDestroyShaderModule(m_device, module, nullptr);

   return {
      .pipeline = pipeline,
      .layout = layout,
   };
}

void VulkanDevice::destroyPipeline(Pipeline& pipeline) {
   vkDestroyPipelineLayout(m_device, pipeline.layout, nullptr);
   vkDestroyPipeline(m_device, pipeline.pipeline, nullptr);
}

VkFence VulkanDevice::createFence() {
   VkFenceCreateInfo fenceCreateInfo = {
      .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
   };
   VkFence fence;
   CHECK_VKRESULT(vkCreateFence(m_device, &fenceCreateInfo, nullptr, &fence));

   return fence;
}

void VulkanDevice::destroyFence(VkFence fence) {
   vkDestroyFence(m_device, fence, nullptr);
}

VulkanDevice::~VulkanDevice() {
   vkDestroyDevice(m_device, nullptr);
   vkDestroyInstance(m_instance, nullptr);
}

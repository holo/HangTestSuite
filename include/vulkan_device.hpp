/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef VULKAN_DEVICE_HPP
#define VULKAN_DEVICE_HPP

#define VK_NO_PROTOTYPES
#include <cstdio>
#include <cstdlib>
#include <volk.h>
#include <vulkan/vulkan.h>

#define CHECK_VKRESULT(expr)                                        \
   do {                                                             \
      VkResult _result = expr;                                      \
      if (_result != VK_SUCCESS) {                                  \
         fprintf(stderr, #expr ": result %d!\n", _result);          \
         abort();                                                   \
      }                                                             \
   } while (0);

enum class QueueType {
   GFX,
   ACE,
};

struct Buffer {
   VkBuffer buffer;
   VkDeviceMemory memory;
   VkDeviceAddress va;
   void* hostMap;
};

struct CommandBuffer {
   VkCommandPool pool;
   VkCommandBuffer buffer;
};

struct Pipeline {
   VkPipeline pipeline;
   VkPipelineLayout layout;
};

class VulkanDevice final {
public:
   VulkanDevice();

   VulkanDevice(const VulkanDevice&) = delete;
   VulkanDevice& operator=(const VulkanDevice&) = delete;
   /* TODO: move constructors are implementable but we shouldn't need them */
   VulkanDevice(VulkanDevice&&) = delete;
   VulkanDevice& operator=(VulkanDevice&&) = delete;

   ~VulkanDevice();

   Buffer createBuffer(VkDeviceSize size);
   void destroyBuffer(Buffer& buffer);

   CommandBuffer createCommandBuffer(QueueType type);
   void destroyCommandBuffer(CommandBuffer& commandBuffer);

   Pipeline createPipeline(uint32_t *spv, uint32_t spvSize, uint32_t pushConstantSize);
   void destroyPipeline(Pipeline& pipeline);

   VkFence createFence();
   void destroyFence(VkFence fence);

   VkDevice device() { return m_device; }
   VkQueue gfxQueue() { return m_gfxQueue; }
   VkQueue aceQueue() { return m_aceQueue; }

   const VkPhysicalDeviceProperties& properties() const { return m_properties; }

private:
   VkInstance m_instance;

   VkPhysicalDevice m_physDevice;
   VkPhysicalDeviceProperties m_properties;

   uint32_t m_memTypeIdx;

   VkDevice m_device;

   uint32_t m_gfxFamilyIdx;
   VkQueue m_gfxQueue;
   uint32_t m_aceFamilyIdx;
   VkQueue m_aceQueue;
};

#endif /* VULKAN_DEVICE_HPP */
